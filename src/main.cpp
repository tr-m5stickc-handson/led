#include <Arduino.h>
#include <M5Unified.h>

void setup() {
    auto config = M5.config();
    M5.begin(config);

    M5.Lcd.setBrightness(80);
    M5.Lcd.setTextSize(3);
}

static unsigned int counter = 0;

void loop() {
    M5.Power.setLed(255); // LED 点灯
    M5.Lcd.setCursor(0, 0);
    M5.Lcd.print("ON ");
    delay(1000);
    M5.Power.setLed(0); // LED 消灯
    M5.Lcd.setCursor(0, 0);
    M5.Lcd.print("OFF");
    delay(1000);
}